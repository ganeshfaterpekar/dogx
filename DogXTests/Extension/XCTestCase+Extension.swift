//
//  XCTestCase+Extension.swift
//  DogXTests
//
//  Created by Ganesh Faterpekar on 24/2/22.
//
import XCTest

extension XCTestCase {
    func loadDataBundle(name: String, ext: String) -> Data {
        let bundle = Bundle(for: classForCoder)
        let url = bundle.url(forResource: name, withExtension: ext)
        return try! Data(contentsOf: url!)
    }
}
