//
//  MockDogBreedService.swift
//  DogXTests
//
//  Created by Ganesh Faterpekar on 26/2/22.
//

import Foundation

class MockDogBreedService : DogBreedService {
    var mockRequestManager: MockRequestManager?
    
    override func getDogBreedList(limit: Int = 0, completionHandler: @escaping (DogBreedList?, Error?) -> Void) {
        mockRequestManager?.getDogBreedList(limit: limit, completion: {  dogBreedlist, error  in
            completionHandler(dogBreedlist,nil)
        })
    }
}
