//
//  MockRequestManager.swift
//  DogXTests
//
//  Created by Ganesh Faterpekar on 26/2/22.
//

import Foundation

class MockRequestManager: RequestManager {
    var mockNetworkService : MockNetworkService?
    
    override func getDogBreedList(limit: Int, completion: @escaping (DogBreedList?, String?) -> Void) {
        mockNetworkService?.request(.getDogBreadList(limit: limit)) { data, response, error in
            if let response = response as? HTTPURLResponse {
               let result = self.handleNetworkResponse(response: response)
                switch result {
                case .success:
                        guard let responseData = data else {
                        completion(nil,NetworkResponse.noData.rawValue)
                        return
                    }
                    do {
                        let apiResponse = try JSONDecoder().decode(DogBreedList.self, from: responseData)
                        completion(apiResponse,nil)
                    } catch {
                        completion(nil,NetworkResponse.parsingError.rawValue)
                    }
                case .failure(let networkError):
                        completion(nil,networkError)
                    }
            }
        }
    }
}

