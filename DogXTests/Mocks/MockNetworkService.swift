//
//  MockNetworkService.swift
//  DogXTests
//
//  Created by Ganesh Faterpekar on 26/2/22.
//

import Foundation

class MockNetworkService: NetworkService {
    var data : Data?
    var error: Error?
    var delay : TimeInterval = 0.0
    var urlResponse: URLResponse?
    
    override func request(_ route: RequestEndPoint, completion: @escaping NetworkServiceCompletionHandler) {
        if let data = self.data {
            DispatchQueue.main.asyncAfter(deadline: .now()+delay) {
                completion(data,self.urlResponse,nil)
           }
        } else if let error = error {
            completion(nil,nil,error)
        }
    }
}
