//
//  DogBreedServiceTest.swift
//  DogXTests
//
//  Created by Ganesh Faterpekar on 27/2/22.
//

import XCTest

class DogBreedServiceTest: XCTestCase {

    var mockDogService : MockDogBreedService?
    var data : Data?

    override func setUpWithError() throws {
        let response = HTTPURLResponse(url: URL(string: "https://api.thedogapi.com/v1/images/search")!,
            statusCode: 200, httpVersion: "HTTP/1.1", headerFields: nil)!
        
        mockDogService =  MockDogBreedService()
        mockDogService?.mockRequestManager = MockRequestManager()
        mockDogService?.mockRequestManager?.mockNetworkService = MockNetworkService()
        mockDogService?.mockRequestManager?.mockNetworkService?.data =  loadDataBundle(name: "dog", ext: "json")
        mockDogService?.mockRequestManager?.mockNetworkService?.urlResponse = response
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testServiceReturns_CorrectData() throws {
        let expectation = XCTestExpectation(description: "Test Nework Service")
        mockDogService?.getDogBreedList { data , _ in
            if let data = data {
                XCTAssertEqual(data.count, 20)
                expectation.fulfill()
            }
        }
        wait(for: [expectation], timeout: 5.0)
    }

}
