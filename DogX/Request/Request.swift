//
//  Request.swift
//  DogX
//
//  Created by Ganesh Faterpekar on 26/2/22.
//

import Foundation

public typealias HTTPHeaders = [String:String]

protocol RequestType {
    var baseURI: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
}

public enum HTTPMethod : String {
    case GET = "GET"
    case POST = "POST"
    case PUT = "PUT"
    case PATCH = "PATCH"
    case DElETE = "DELETE"
}

public enum HTTPTask {
    case request
    case requestParameters(bodyParams: Parameters? , urlParams: Parameters?)
}

