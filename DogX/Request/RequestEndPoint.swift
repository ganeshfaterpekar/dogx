//
//  RequestEndPoint.swift
//  DogX
//
//  Created by Ganesh Faterpekar on 26/2/22.
//

import Foundation

enum NetworkEnvironment {
    case developement

}

public enum RequestEndPoint {
    case getDogBreadList( limit: Int)
}

extension RequestEndPoint: RequestType {

    var baseUrlBaseForEnvironment: String {
        switch RequestManager.enivornment {
        case .developement:
             return "https://api.thedogapi.com"
        }
    }
    
    var baseURI: URL {
        guard let url = URL(string: baseUrlBaseForEnvironment) else {fatalError("URL cannot be constructed")}
        return url
    }
    
    var path: String {
        switch self {
        case .getDogBreadList(_):
            return "/v1/images/search"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .GET
    }
    
    var task: HTTPTask {
        switch self {
        case .getDogBreadList(let limit):
            return .requestParameters(bodyParams: nil, urlParams: ["limit":limit])
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
}

