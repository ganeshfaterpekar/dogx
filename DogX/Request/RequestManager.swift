//
//  RequestManager.swift
//  DogX
//
//  Created by Ganesh Faterpekar on 26/2/22.
//

import Foundation

class RequestManager {
    static let enivornment : NetworkEnvironment = .developement
    var networkService = NetworkService()
    
    func getDogBreedList(limit: Int, completion:@escaping (DogBreedList?, String?) -> Void ) {
        networkService.request(.getDogBreadList(limit: limit)) { data, response, _ in
            if let response = response as? HTTPURLResponse {
               let result = self.handleNetworkResponse(response: response)
                switch result {
                case .success:
                        guard let responseData = data else {
                        completion(nil,NetworkResponse.noData.rawValue)
                        return
                    }
                    do {
                        let apiResponse = try JSONDecoder().decode(DogBreedList.self, from: responseData)
                        completion(apiResponse,nil)
                    } catch {
                        completion(nil,NetworkResponse.parsingError.rawValue)
                }
                case .failure(let networkError):
                        completion(nil,networkError)
                 }
            }
        }
    }
    
    func handleNetworkResponse( response: HTTPURLResponse) -> Result<String> {
        switch response.statusCode {
            case 200...299:
                return .success
            case 401...500:
                return .failure(NetworkResponse.authError.rawValue)
            case 501...599:
                return .failure(NetworkResponse.badRequest.rawValue)
            default:
                return .failure(NetworkResponse.failedRequest.rawValue)
      }
    }
    
    enum NetworkResponse: String {
        case success
        case authError = ""
        case badRequest = "Bad request"
        case failedRequest = "Network Request failed"
        case noData = "Response with No Data"
        case parsingError = "Unable to Decode the data"
    }

    enum Result<Message> {
        case success
        case failure(Message)
    }

}
