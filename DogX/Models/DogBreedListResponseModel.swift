//
//  DogBreedListResponseModel.swift
//  DogX
//
//  Created by Ganesh Faterpekar on 24/2/22.
//

import Foundation
typealias DogBreedList = [DogBreedListResponseModel]

struct DogBreedListResponseModel: Decodable {
    let breeds: [DogBreed]?
    let url: String?
}

struct DogBreed: Decodable {
    let name: String?
    let lifeSpan: String?

    enum CodingKeys: String, CodingKey {
        case name
        case lifeSpan = "life_span"
    }

}

extension DogBreedListResponseModel {
    var urlValue: String {
        guard let url = url else {
            return ""
        }
        return url
    }
}

extension DogBreed {
    
    var lifeSpanValue: String {
        guard let lifespan = lifeSpan else {
            return ""
        }
        return lifespan
    }
    
    var nameValue: String {
        guard let name = name else {
            return ""
        }
        return name
    }
    
    var lifeSpanMaxValues: Int {
        guard let lifespanString = lifeSpan else {
            return 0
        }
    
        let component = lifespanString.components(separatedBy: NSCharacterSet.decimalDigits.inverted)
        let list = component.filter({ $0 != ""}) // filter out all the empty strings in the component
        if list.count > 1 {
            return Int(list[1]) ?? 0
        }
        return Int(list[0]) ?? 0
 
    }
}

