//
//  DogBreedCellViewModel.swift
//  DogX
//
//  Created by Ganesh Faterpekar on 27/2/22.
//

import Foundation

struct DogBreedCellViewModel {
    var name: String = ""
    var imagUrl: String = ""
    var lifeSpan: String = ""
    var maxlifeSpan: Int = 0
    
    var lifeSpanString : String {
        if lifeSpan != "" {
            return "Life Span: \(lifeSpan)"
        }
        return lifeSpan
    }
}
