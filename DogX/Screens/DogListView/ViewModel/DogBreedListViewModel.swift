//
//  DogBreedListViewModel.swift
//  DogX
//
//  Created by Ganesh Faterpekar on 27/2/22.
//

import Foundation
class DogBreedListViewModel {
    private var dogBreedService : DogBreedServiceProtocol
    
    var reloadTableView : (() -> Void)?
    var dogBreedCellViewModels = [DogBreedCellViewModel]() {
        didSet {
            reloadTableView?()
        }
    }

    
    init(dogBreedService: DogBreedServiceProtocol = DogBreedService()) {
        self.dogBreedService = dogBreedService
    }
    
    func getDogBreedList() {
        dogBreedService.getDogBreedList(limit: 50) { data, _ in
            if let dogBreedList = data {
                self.processData(dogList: dogBreedList)
            }
        }
        
    }
    
    func processData(dogList: DogBreedList) {
        var cells = [DogBreedCellViewModel]()
        for dog in dogList {
            let cellModel = createCellModel(dogBread: dog)
            if isDogVMValid(dog: cellModel) {
               cells.append(cellModel)
            }
        }
        let sortedCells = cells.sorted { $0.maxlifeSpan < $1.maxlifeSpan }
        dogBreedCellViewModels = sortedCells
    }
    
    func createCellModel(dogBread: DogBreedListResponseModel) -> DogBreedCellViewModel {
        
        let imageUrl = dogBread.urlValue
        guard let breed = dogBread.breeds else {
            return DogBreedCellViewModel(imagUrl: imageUrl)
        }
        
        if !breed.isEmpty {
            let name = breed[0].nameValue
            let lifeSpan = breed[0].lifeSpanValue
            return DogBreedCellViewModel(name: name,
                                         imagUrl: imageUrl,
                                         lifeSpan: lifeSpan ,
                                         maxlifeSpan: breed[0].lifeSpanMaxValues)
        }
        return DogBreedCellViewModel()
    }
    
    
    func getCellViewModel(at indexPath: IndexPath) -> DogBreedCellViewModel {
        return dogBreedCellViewModels[indexPath.row]
    }
    
    func sortDogBreedBasedOn(sortBy: SortBY) {
        let currentCells = dogBreedCellViewModels
        switch sortBy {
        case .lifeSpanASC:
            dogBreedCellViewModels = currentCells.sorted { $0.maxlifeSpan < $1.maxlifeSpan }
        case .lifeSpanDSC:
            dogBreedCellViewModels = currentCells.sorted { $0.maxlifeSpan > $1.maxlifeSpan }
        }
    }
    
    func isDogVMValid(dog : DogBreedCellViewModel) -> Bool {
        if (dog.imagUrl  == "" && dog.name == "" ) {
            return false
        }
        return true
    }
    
    
}

enum SortBY : String {
    case lifeSpanASC
    case lifeSpanDSC
}

