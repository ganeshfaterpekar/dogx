//
//  DogListViewController.swift
//  DogX
//
//  Created by Ganesh Faterpekar on 27/2/22.
//

import Foundation
import UIKit

class DogListViewController: UIViewController {
    
    @IBOutlet var dogListTableView: UITableView!
    lazy var viewModel = {
       DogBreedListViewModel()
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "Dog list"
        initTableView()
        initTableViewModel()
    }

    func initTableView() {
        dogListTableView.delegate = self
        dogListTableView.dataSource = self
        dogListTableView.separatorStyle = .singleLine
        dogListTableView.separatorColor = .gray
        dogListTableView.allowsSelection = false

        dogListTableView.register(DogsDetailsCell.nib, forCellReuseIdentifier: DogsDetailsCell.identifier)
        dogListTableView.register(DogCellSortHeaderView.nib, forHeaderFooterViewReuseIdentifier: DogCellSortHeaderView.identifier)
    }

    func initTableViewModel() {
        viewModel.getDogBreedList()
        viewModel.reloadTableView = { [weak self] in
            DispatchQueue.main.async {
                self?.dogListTableView.reloadData()
            }
        }
    }

}

extension DogListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.dogBreedCellViewModels.isEmpty {
                self.dogListTableView.setEmptyMessage("Loading Dogs...")
            } else {
                self.dogListTableView.restore()
        }
        return viewModel.dogBreedCellViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DogsDetailsCell.identifier, for: indexPath) as? DogsDetailsCell else {
            fatalError("DogsDetailsCell does not exsists")
        }
        let cellVM = viewModel.getCellViewModel(at: indexPath)
        cell.cellViewModel = cellVM
        
        return cell
    }
}

extension DogListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if !viewModel.dogBreedCellViewModels.isEmpty {
           return 50
        }
           
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      guard let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: DogCellSortHeaderView.identifier) as? DogCellSortHeaderView
            else { fatalError("DogCellSortHeaderView does not exsists") }
       headerView.sortTable = { [weak self] sortby  in self?.viewModel.sortDogBreedBasedOn(sortBy: sortby) }
      return headerView
    }
}

extension UITableView {

    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "TrebuchetMS", size: 15)
        messageLabel.sizeToFit()

        self.backgroundView = messageLabel
        self.separatorStyle = .none
    }

    func restore() {
        self.backgroundView = nil
        self.separatorStyle = .singleLine
    }
}
