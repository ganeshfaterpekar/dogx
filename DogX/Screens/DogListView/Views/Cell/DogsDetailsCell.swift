//
//  DogsDetailsCell.swift
//  DogX
//
//  Created by Ganesh Faterpekar on 27/2/22.
//

import Foundation
import UIKit
import Kingfisher
class DogsDetailsCell: UITableViewCell {
    @IBOutlet var dogImageView: UIImageView!
    @IBOutlet var breedNameLabel: UILabel!
    @IBOutlet var lifeSpan: UILabel!
    @IBOutlet var heightUnitsLabel: UILabel!
    
    var cellViewModel : DogBreedCellViewModel? {
        didSet {
            dogImageView.kf.setImage(with: URL(string: cellViewModel!.imagUrl), options: [
                .loadDiskFileSynchronously,
                .cacheOriginalImage,
                .transition(.fade(0.25))
            ])
            breedNameLabel.text = cellViewModel?.name
            lifeSpan.text = cellViewModel?.lifeSpanString
        }
    }
    
    class var identifier: String { return String(describing: self) }
    class var nib: UINib { return UINib(nibName: identifier, bundle: nil) }
    
    override func awakeFromNib() {
         super.awakeFromNib()
        initCellView()
    }
    
    func initCellView() {
        separatorInset = UIEdgeInsets.zero
        layoutMargins = UIEdgeInsets.zero
        backgroundColor = .clear
        preservesSuperviewLayoutMargins = false
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        dogImageView.image = nil
        breedNameLabel.text = nil
        lifeSpan.text = nil
    }
    
}
