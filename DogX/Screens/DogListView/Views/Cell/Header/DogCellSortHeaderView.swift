//
//  DogCellSortHeaderView.swift
//  DogX
//
//  Created by Ganesh Faterpekar on 27/2/22.
//

import UIKit

class DogCellSortHeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var sortButton : UIButton!
    @IBOutlet weak var sortingLabel : UILabel!
    
    var sortTable : ((_ sortType : SortBY) -> Void)?
    var currentSortType : SortBY = .lifeSpanASC
    
    static var nib: UINib { return UINib(nibName: identifier, bundle: nil) }
    static var identifier: String { return String(describing: self) }
    
    @IBAction func buttonTapped(_ sender : UIButton) {

        if currentSortType == .lifeSpanASC {
            currentSortType = .lifeSpanDSC
            sortingLabel.text = "Life Span - DSC"
        } else {
            currentSortType = .lifeSpanASC
            sortingLabel.text = "Life Span - ASC"
        }
        
        sortTable?(currentSortType)
    }
}
 
