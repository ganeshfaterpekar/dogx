//
//  NetworkService.swift
//  DogX
//
//  Created by Ganesh Faterpekar on 24/2/22.
//

import Foundation

public typealias NetworkServiceCompletionHandler = (_ data: Data? , _ response: URLResponse? , _ error: Error?) -> Void


protocol NetworkServiceProcol {
    func request(_ requestEndPoint: RequestEndPoint , completion: @escaping NetworkServiceCompletionHandler)
    func cancel()
}


class NetworkService: NetworkServiceProcol {
    private var sessionTask: URLSessionTask?
    
    func request(_ requestEndPoint: RequestEndPoint, completion: @escaping NetworkServiceCompletionHandler) {
        let session = URLSession.shared
        do {
             let request  = try self .buildRequest(from: requestEndPoint)
            self.sessionTask = session.dataTask(with: request, completionHandler: { data, response, error in
                completion(data,response,error)
            })
            
        } catch {
            completion(nil,nil,error)
        }
        
        self.sessionTask?.resume()
    }
    
    func cancel() {
        
    }
    
    
    func buildRequest(from requestType: RequestEndPoint) throws -> URLRequest {
        var request = URLRequest(url: requestType.baseURI.appendingPathComponent(requestType.path),
                                 cachePolicy: .reloadIgnoringLocalAndRemoteCacheData,
                                 timeoutInterval: 60.0)
        
        request.httpMethod = requestType.httpMethod.rawValue
        
        do {
          switch requestType.task {
          case .request:
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
          case .requestParameters( let bodyParams,let urlParams):
                return try encode(request: request, parameters: urlParams!)
              
          }
        } catch {
            throw error
        }
        
        return request
    }
    
}

public typealias Parameters = [String:Any]

public enum NetworkError: String, Error {
    case parametersAreNIL = "Parameters are missing"
    case encodingFailed = "Encoding failed"
    case httpErrors = "Networking error"
    case missingURL = "URL is missing"
}


public func encode(request: URLRequest,  parameters: Parameters) throws ->  URLRequest{
    var urlRequest = request
    guard let url = urlRequest.url else { throw NetworkError.missingURL }
    
    if var urlComponents = URLComponents(url: url,resolvingAgainstBaseURL: false), !parameters.isEmpty {
           urlComponents.queryItems = [URLQueryItem]()
        
        for (key,value) in parameters {
            let queryItem = URLQueryItem(name: key,
                                        value: "\(value)".addingPercentEncoding(withAllowedCharacters: .urlHostAllowed))
            urlComponents.queryItems?.append(queryItem)
        }
            urlRequest.url = urlComponents.url
    }
    
    if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
        urlRequest.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
    }
    
    return urlRequest
    
}
