//
//  DogBreedService.swift
//  DogX
//
//  Created by Ganesh Faterpekar on 26/2/22.
//

import Foundation

protocol DogBreedServiceProtocol {
    func getDogBreedList(limit: Int, completionHandler: @escaping (DogBreedList? , Error?) -> Void )
}

class DogBreedService : DogBreedServiceProtocol {
     var  requestManager: RequestManager
    
    public init() {
      self.requestManager = RequestManager()
    }
    
    public init(manager: RequestManager) {
       self.requestManager = manager
    }
    
    func getDogBreedList(limit: Int = 0 ,completionHandler: @escaping (DogBreedList?, Error?) -> Void) {
        self.requestManager.getDogBreedList(limit: limit) { dogBreedlist, _ in
            if let dogBreedlist = dogBreedlist {
                completionHandler(dogBreedlist,nil)
            }
        }
    }
    
}
