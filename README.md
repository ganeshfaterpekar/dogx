# DogX

## Overview

Develop a native iOS application that makes a call to a simple (RESTful) web service, and displays the data that is returned in the response on the screen of the mobile app.

## Implementation Requirements
- API to be consumed - https://api.thedogapi.com/v1/images/search?limit=50
- On App launch make a call to the API to retrieve the data
- Display the data in a UITableView.(At a minimum display name, life-span, image)
- A button at top of tableview allows data to be sorted ascending/descending by life-span


## System Requirements

* Xcode 11.x
* iOS 12
* Swift Package Manager

## Setup

```
git clone https://gitlab.com/ganeshfaterpekar/dogx.git

```

### Architecture
## Code 
Code follows MVVM design pattern combined with SOLID Principles. The code is divided into different group / layers (Self explanatory)         

## Third Party
Kingfisher library used for image loading



## Requirement Traceability


| Requirement                        |   Implemented        | Comments                                           |
| -------------                      |    :-------------:   | :-----                                             |
| Swift langauge                     | :white_check_mark:    | Done using Swift    |
| Xcode 12                           | :white_check_mark:    |                     |
| Desgin Patterns                    | :white_check_mark:    | Solid Principles,MVVM, Dependency Injection followed |
| Dependency Management              | :white_check_mark:    | SPM   |
| Testing                            | :white_check_mark:    | Unit testing Added |

